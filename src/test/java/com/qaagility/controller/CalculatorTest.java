package com.qaagility.controller;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalculatorTest {
    @Test
    public void testCalculatorAdd (){
        final Calculator underTest = new Calculator();
        final int actualResult = underTest.add();

        assertEquals("Test Calculator Add", 9, actualResult);

    }
}

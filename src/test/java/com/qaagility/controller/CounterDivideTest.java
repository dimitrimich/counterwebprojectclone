package com.qaagility.controller;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CounterDivideTest {
    @Test
    public void testDMethodEqualsZero(){
        final CounterDivide underTest = new CounterDivide();

        final int actualResult = underTest.divide(5, 0);
        assertEquals("Test D method (0)", Integer.MAX_VALUE, actualResult);
    }
    @Test
    public void testDMethodNonZero(){
        final CounterDivide underTest = new CounterDivide();

        final int actualResult = underTest.divide(5, 5);
        assertEquals("Test D method (5)", 1, actualResult);
    }
}

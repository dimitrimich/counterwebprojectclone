package com.qaagility.controller;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class AboutTest {

    @Test
    public void testDesc(){

        final String expectedResult = "This application was copied";
        final About classUnderTest = new About();

        final String actualResult = classUnderTest.desc();

        assertTrue("testDesc", actualResult.contains(expectedResult));


    }
}

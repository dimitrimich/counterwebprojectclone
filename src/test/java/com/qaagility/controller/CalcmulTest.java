package com.qaagility.controller;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalcmulTest {
    @Test
    public void testMul(){
        final Calcmul underTest = new Calcmul();
        final int actualResul = underTest.mul();

        assertEquals("Test Mul", 18, actualResul);
    }
}

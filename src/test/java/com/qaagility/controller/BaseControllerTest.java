package com.qaagility.controller;

import static org.mockito.Mockito.*;
import org.junit.Test;
import org.springframework.ui.ModelMap;


public class BaseControllerTest {

	@Test
	public void testWelcome() {
		final ModelMap map = mock(ModelMap.class);
		final String result = new BaseController().welcome(map);
		verify(map).addAttribute(eq("message"), startsWith("Welcome - "));
		verify(map).addAttribute(eq("counter"), anyInt());
	}

	@Test
	public void testWelcomeName() {
		final ModelMap map = mock(ModelMap.class);
		final String result = new BaseController().welcomeName("test", map);
		verify(map).addAttribute(eq("message"), startsWith("Welcome - test -"));
		verify(map).addAttribute(eq("counter"), anyInt());
	}
}

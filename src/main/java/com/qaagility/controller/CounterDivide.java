package com.qaagility.controller;

public class CounterDivide {

    public int divide(int firstNumber, int secondNumber) {
        if (secondNumber == 0) {
            return Integer.MAX_VALUE;
        }
        else {
            return firstNumber / secondNumber;
        }
    }

}
